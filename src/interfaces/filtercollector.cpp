/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "filtercollector.h"

#include "imagefilter.h"

#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QPluginLoader>

class FilterCollector::FilterCollectorPrivate
{

public:

    FilterCollectorPrivate( FilterCollector *collector )
        : filter(),
          filterCollector( collector )
    {
        loadFilters();
    }

    virtual ~FilterCollectorPrivate()
    {

    }

    QList< ImageFilter * > filter;

private:

    FilterCollector *filterCollector;

    void loadFilters()
    {
        qDebug() << "FilterCollectorPrivate: Checking for static image filters.";
        foreach ( QObject *plugin, QPluginLoader::staticInstances() )
        {
            if ( checkAndAddPlugin( plugin ) )
            {
                qDebug() << "Added static image filter plugin.";
            }
        }
        QDir pluginsDir = QDir( qApp->applicationDirPath() );
        pluginsDir.cd("plugins");
        qDebug() << "FilterCollectorPrivate: Searching for plugins in" << pluginsDir.absolutePath();
        foreach ( QString pluginFile, pluginsDir.entryList( QDir::Files ) )
        {
            qDebug() << "FilterCollectorPrivate: Testing file" << pluginFile;
            QPluginLoader loader( pluginsDir.absoluteFilePath( pluginFile ) );
            QObject *plugin = loader.instance();
            if (plugin)
            {
                qDebug() << "FilterCollectorPrivate: Loaded plugin. Testing for ImageFilter interface...";
                if ( checkAndAddPlugin( plugin ) )
                {
                    qDebug() << "FilterCollectorPrivate: Loaded ImageFilter plugin.";
                } else
                {
                    qDebug() << "FilterCollectorPrivate: Not an ImageFilter plugin!";
                }
            } else
            {
                qWarning() << "FilterCollectorPrivate: Failed to load plugin file!";
            }
        }
    }

    bool checkAndAddPlugin( QObject *plugin )
    {
        ImageFilter *filter = qobject_cast< ImageFilter * >( plugin );
        if ( filter )
        {
            filter->setCollection( filterCollector );
            this->filter.append( filter );
            return true;
        }
        return false;
    }
};

FilterCollector::FilterCollector( QObject *parent )
    : QObject( parent ),
      d( new FilterCollectorPrivate( this ) )
{
}

FilterCollector::~FilterCollector()
{
    delete d;
}

ImageFilter *FilterCollector::filter( const QString &name ) const
{
    foreach ( ImageFilter *filter, d->filter )
    {
        if ( filter->name() == name )
        {
            return filter;
        }
    }
    return 0;
}

const QStringList FilterCollector::filterNames() const
{
    QStringList result;
    foreach ( ImageFilter *filter, d->filter )
    {
        result << filter->name();
    }
    return result;
}
