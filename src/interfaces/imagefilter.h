/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IMAGEFILTER_H
#define IMAGEFILTER_H

#include "imagefilterdefinitions.h"

#include <QString>
#include <QHash>
#include <QVariant>

class FilterCollector;

/**
  * @brief Interface for image filters.
  *
  * All image filters must implement this interface.
  **/
class IFI_EXPORT ImageFilter
{

public:

    /**
      * @brief Destructor.
      */
    virtual ~ImageFilter() {};

    /**
      * @brief The filter name.
      *
      * The name of the filter. This is used to identify the filter, e.g. when invoking it from
      * command line.
      */
    virtual const QString name() const = 0;

    /**
      * @brief Set collection context.
      *
      * This is called from the FilterCollection when the filter is added to it.
      * Filters should use this to set the collection as a context to be able to access
      * the other parts of the application.
      *
      * @param collection The collection the filter has been added to.
      */
    virtual void setCollection( FilterCollector *collection ) = 0;

    /**
      * @brief Supported parameters.
      *
      * Returns a list with all parameters processed by the filter.
      */
    virtual const QStringList parameters() const = 0;

    /**
      * @brief Parameter descriptions.
      *
      * Returns a description for the requested parameter.
      *
      * @param parameterName The name of the parameter to get the description for.
      */
    virtual const QString parameterDescription( const QString &parameterName ) const = 0;

    /**
      * @brief Execute the filter.
      *
      * Executes the filter with the given parameters. No user interaction is allowed.
      *
      * @param params The parameters to use for processing.
      *
      * @returns True on success, otherwise false.
      */
    virtual bool execute( const ParameterHash &params ) = 0;

    /**
      * @brief Execute the filter.
      *
      * This is called when the filter is invoked from the GUI. Usually, a filter will
      * show a dialog so that the user can make the settings he usually would pass in by
      * the parameter hash.
      *
      * @returns True on success, otherwise false.
      */
    virtual bool executeFromGui() = 0;

    /**
      * @brief Reserved.
      *
      * This is reserved for future use. More specifically, it might be used for extended functionality (e.g.
      * letting the filter return an icon the GUI can display along the menu entry for the filter or
      * such things.
      */
    virtual QVariant data( int what ) const = 0;
};

Q_DECLARE_INTERFACE( ImageFilter , "net.rpdev.kpki2010.imagefilter/1.0" );

#endif // IMAGEFILTER_H
