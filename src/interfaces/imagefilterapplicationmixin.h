/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IMAGEFILTERAPPLICATIONMIXIN_H
#define IMAGEFILTERAPPLICATIONMIXIN_H

#include "imagefilterdefinitions.h"

#include <QObject>

class FilterCollector;
class ImageCollector;

/**
  * @brief Mixin for application classes.
  *
  * This class provides a kind of mixin for the QCoreApplication (or its derivation)
  * used in the client app. The mixin can be additionally derived from to
  * draw in the ImageFilter capabilities.
  * It is required to create a own QCoreApplication subclass with this mixin
  * as filters will expect the QCoreApplication::instance() to be (dynamically)
  * castable to ImageFilterApplicationMixin.
  */
class IFI_EXPORT ImageFilterApplicationMixin
{
public:

    /**
      * @brief Ctor.
      */
    ImageFilterApplicationMixin();

    /**
      * @brief Dtor.
      */
    virtual ~ImageFilterApplicationMixin();

    /**
      * @brief Initialize the mixin.
      *
      * This will initialize internal stuff of the mixin. More specifically, this
      * will create the different collectors required to run the image filter application.
      *
      * @param parent A QObject which will be used as the parent object for the
      *               collectors.
      */
    void initializeImageFilterApplication( QObject *parent = 0 );

    /**
      * @brief Returns the image collector of the application.
      **/
    ImageCollector *imageCollector() const;

    /**
      * @brief Returns the filter collector of the application.
      */
    FilterCollector *filterCollector() const;

    /**
      * @brief Access to the image filter application mixin.
      *
      * This method will try to cast the global QCoreApplication instance to
      * the image filter mixin.
      * Filter plugins should use this to access the collectors of
      * the application.
      */
    static ImageFilterApplicationMixin *imageFilterApplication();

private:

    class ImageFilterApplicationMixinPrivate;
    ImageFilterApplicationMixinPrivate *d;

};

/**
  * @brief Access the global ImageFilterApplicationMixin.
  *
  * This is a shortcut for the accessing the gloal ImageFilterApplicationMixin.
  */
#define imageFilterApp ImageFilterApplicationMixin::imageFilterApplication()

#endif // IMAGEFILTERAPPLICATIONMIXIN_H
