/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IMAGEFILTERDEFINITIONS_H
#define IMAGEFILTERDEFINITIONS_H

#include <QHash>
#include <QImage>
#include <QString>
#include <QtGlobal>

#ifdef IMAGE_FILTER_INTERFACES
#define IFI_EXPORT Q_DECL_EXPORT
#else
#define IFI_EXPORT Q_DECL_IMPORT
#endif

/**
  * @brief Hash used to pass parameters to an image filter.
  *
  * This class is usually used to pass parameters to an image filter when calling it
  * to do some processing.
  */
class ParameterHash : public QHash< QString, QString > {};

/**
  * @brief Image storage.
  *
  * This hash class maps names to images and thus is used as storage for
  * the images loaded and generated.
  */
class ImageHash : public QHash< QString, QImage > {};


#endif // IMAGEFILTERDEFINITIONS_H
