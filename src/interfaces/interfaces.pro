TEMPLATE = lib
CONFIG += plugin \
    core \
    gui
TARGET = imagefilterinterfaces
HEADERS += imagefilter.h \
    imagefilterdefinitions.h \
    filtercollector.h \
    imagecollector.h \
    imagefilterapplicationmixin.h
CONFIG(debug, debug|release):DESTDIR = ../../bin/Debug
else:DESTDIR = ../../bin/Release
SOURCES += filtercollector.cpp \
    imagecollector.cpp \
    imagefilterapplicationmixin.cpp
DEFINES += IMAGE_FILTER_INTERFACES
