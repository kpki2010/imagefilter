/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FILTERCOLLECTOR_H
#define FILTERCOLLECTOR_H

#include "imagefilterdefinitions.h"

#include <QObject>
#include <QStringList>

class ImageFilter;

/**
  * @brief Collects image filters.
  *
  * The FilterCollector is responsible of loading and managing filter plugins.
  * It provides an interface to access the available image filters.
  * On creation, it searches the plugins directory in the application path
  * for available dynamic plugins. Additionally, it also looks for statically linked
  * in plugins and loads them also.
  */
class IFI_EXPORT FilterCollector : public QObject
{

    Q_OBJECT

public:

    /**
      * @brief Ctor.
      */
    FilterCollector( QObject *parent = 0 );

    /**
      * @brief Dtor.
      */
    virtual ~FilterCollector();

    /**
      * @brief Returns named image filter.
      *
      * Returns an image filter by its name or 0, if no such filter is loaded.
      *
      * @param name The filter name.
      *
      * @returns A filter which returns name when its name() method is called or 0
      *          if no such filter exists.
      */
    ImageFilter *filter( const QString &name ) const;

    /**
      * @brief Loaded filters.
      *
      * Returns a list with all filter's names.
      */
    const QStringList filterNames() const;

private:

    class FilterCollectorPrivate;
    FilterCollectorPrivate *d;

};

#endif // FILTERCOLLECTOR_H
