/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "imagefilterapplicationmixin.h"

#include "filtercollector.h"
#include "imagecollector.h"

#include <QApplication>

class ImageFilterApplicationMixin::ImageFilterApplicationMixinPrivate
{

public:

    ImageFilterApplicationMixinPrivate( ImageFilterApplicationMixin *mixin )
        : filterCollector( 0 ),
          imageCollector( 0 ),
          imageFilterApplicationMixin( mixin )
    {
    }

    virtual ~ImageFilterApplicationMixinPrivate()
    {
    }

    FilterCollector *filterCollector;
    ImageCollector *imageCollector;

private:

    ImageFilterApplicationMixin *imageFilterApplicationMixin;

};

ImageFilterApplicationMixin::ImageFilterApplicationMixin()
    : d( new ImageFilterApplicationMixinPrivate( this ) )
{
}

ImageFilterApplicationMixin::~ImageFilterApplicationMixin()
{
    delete d;
}

void ImageFilterApplicationMixin::initializeImageFilterApplication( QObject *parent )
{
    d->filterCollector = new FilterCollector( parent );
    d->imageCollector = new ImageCollector( parent );
}

FilterCollector *ImageFilterApplicationMixin::filterCollector() const
{
    return d->filterCollector;
}

ImageCollector *ImageFilterApplicationMixin::imageCollector() const
{
    return d->imageCollector;
}

ImageFilterApplicationMixin *ImageFilterApplicationMixin::imageFilterApplication()
{
    return dynamic_cast< ImageFilterApplicationMixin * >( QCoreApplication::instance() );
}
