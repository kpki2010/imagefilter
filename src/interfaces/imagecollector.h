/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IMAGECOLLECTOR_H
#define IMAGECOLLECTOR_H

#include "imagefilterdefinitions.h"

#include <QImage>
#include <QObject>
#include <QStringList>

/**
  * @brief Image storage.
  *
  * The ImageCollector is used to hold the named images at runtime.
  * The concept behind is simple: Each image has a name associated. This
  * name is used to identify the image at runtime.
  * Filters can query the image collector for images using that names.
  *
  * Additionally, filters can also store new images in the collector
  * using a new name or override existing ones.
  */
class IFI_EXPORT ImageCollector : public QObject
{

    Q_OBJECT

public:

    /**
      * @brief Ctor.
      */
    ImageCollector( QObject *parent = 0 );

    /**
      * @brief Dtor.
      */
    virtual ~ImageCollector();

    /**
      * @brief Return named image.
      *
      * Returns the image associated with @p name. If no image is assigned that name,
      * an empty image will be returned.
      */
    QImage image( const QString &name ) const;

    /**
      * @brief Create or override image.
      *
      * Creates or overrides the image named @p name.
      */
    void setImage( const QString &name, QImage image );

    /**
      * @brief Delete named image.
      *
      * Deletes the named image referred to by @p name.
      */
    void deleteImage( const QString &name );

    /**
      * @brief Clear the image storage.
      *
      * Delete all images from the image storage.
      * Subsequent calls to image() will return empty images unless
      * new images are added using setImage().
      */
    void clearImages();

    /**
      * @brief List of images.
      *
      * Returns a list with images currently hold by the image collector.
      */
    QStringList images() const;


private:

    class ImageCollectorPrivate;
    ImageCollectorPrivate *d;

};

#endif // IMAGECOLLECTOR_H
