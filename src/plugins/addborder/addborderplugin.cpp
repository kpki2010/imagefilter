/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "addborderplugin.h"

#include "imagecollector.h"
#include "imagefilterapplicationmixin.h"

#include <QColor>
#include <QPainter>
#include <QtPlugin>

Q_EXPORT_PLUGIN2( AddBorderPlugin, AddBorderPlugin )

const QString AddBorderPlugin::NAME = "addborder";

const QString AddBorderPlugin::INPUT                = "input";
const QString AddBorderPlugin::OUTPUT               = "output";
const QString AddBorderPlugin::BORDER_SIZE          = "size";
const QString AddBorderPlugin::BORDER_SIZE_TOP      = "size-top";
const QString AddBorderPlugin::BORDER_SIZE_BOTTOM   = "size-bottom";
const QString AddBorderPlugin::BORDER_SIZE_LEFT     = "size-left";
const QString AddBorderPlugin::BORDER_SIZE_RIGHT    = "size-right";
const QString AddBorderPlugin::BORDER_COLOR         = "color";

const QString AddBorderPlugin::name() const
{
    return NAME;
}

void AddBorderPlugin::setCollection( FilterCollector *collection )
{
    m_filterCollector = collection;
}

const QStringList AddBorderPlugin::parameters() const
{
    QStringList result;
    result << BORDER_SIZE << BORDER_COLOR;
    return result;
}

const QString AddBorderPlugin::parameterDescription( const QString &parameterName ) const
{
    if ( parameterName == INPUT )
    {
        return tr( "The input image." );
    } else if ( parameterName == OUTPUT )
    {
      return tr( "The name to save the output image as." );
    } else if ( parameterName == BORDER_SIZE )
    {
        return tr( "The size of the border in pixels." );
    } else if ( parameterName == BORDER_SIZE_TOP )
    {
        return tr( "The size of the top border in pixels." );
    } else if ( parameterName == BORDER_SIZE_BOTTOM )
    {
        return tr( "The size of the bottom border in pixels." );
    } else if ( parameterName == BORDER_SIZE_LEFT )
    {
        return tr( "The size of the left border in pixels." );
    } else if ( parameterName == BORDER_SIZE_RIGHT )
    {
        return tr( "The size of the right border in pixels." );
    } else if ( parameterName == BORDER_COLOR )
    {
        return tr( "The color of the border." );
    }
    return QString();
}

bool AddBorderPlugin::execute(const ParameterHash &params )
{
    QString input = params.value( INPUT );
    QString output = params.value( OUTPUT );
    int size = params.value( BORDER_SIZE, "0" ).toInt();
    int sizeTop     = params.contains( BORDER_SIZE_TOP )    ? params.value( BORDER_SIZE_TOP, "0" ).toInt()    : size;
    int sizeBottom  = params.contains( BORDER_SIZE_BOTTOM ) ? params.value( BORDER_SIZE_BOTTOM, "0" ).toInt() : size;
    int sizeLeft    = params.contains( BORDER_SIZE_LEFT )   ? params.value( BORDER_SIZE_LEFT, "0" ).toInt()   : size;
    int sizeRight   = params.contains( BORDER_SIZE_RIGHT )  ? params.value( BORDER_SIZE_RIGHT, "0" ).toInt()  : size;
    QColor color( params.value( BORDER_COLOR, "0xffffff" ) );

    QImage image = imageFilterApp->imageCollector()->image( input );
    if ( !( image.isNull() ) )
    {
        QImage withBorder( image.width() + sizeLeft + sizeRight, image.height() + sizeTop + sizeBottom, image.format() );
        withBorder.fill( color.rgba() );
        QPainter p( &withBorder );
        p.drawImage( sizeLeft, sizeTop, image );
        imageFilterApp->imageCollector()->setImage( output, withBorder );
    }
    return true;
}

bool AddBorderPlugin::executeFromGui()
{
    return execute( ParameterHash() );
}

QVariant AddBorderPlugin::data( int what ) const
{
    Q_UNUSED( what );
    return QVariant();
}

