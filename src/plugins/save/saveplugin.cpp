/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "saveplugin.h"

#include "savedialog.h"

#include "imagefilterapplicationmixin.h"
#include "imagecollector.h"

#include <QFileInfo>
#include <QtPlugin>

Q_EXPORT_PLUGIN2( SavePlugin, SavePlugin )

const QString SavePlugin::NAME = "save";

const QString SavePlugin::FILENAME = "filename";
const QString SavePlugin::IMAGE_NAME = "name";


const QString SavePlugin::name() const
{
    return NAME;
}

void SavePlugin::setCollection( FilterCollector *collection )
{
    m_filterCollector = collection;
}

const QStringList SavePlugin::parameters() const
{
    QStringList result;
    result << FILENAME << IMAGE_NAME;
    result.sort();
    return result;
}

const QString SavePlugin::parameterDescription( const QString &parameterName ) const
{
    if ( parameterName == FILENAME )
    {
        return tr( "File to write image to." );
    } else if ( parameterName == NAME )
    {
        return tr( "Image to write." );
    }
    return QString();
}

bool SavePlugin::execute(const ParameterHash &params )
{
    QImage image = imageFilterApp->imageCollector()->image( params.value( IMAGE_NAME, QString() ) );
    if ( !( image.isNull() ) )
    {
        QString fileName = params.value( FILENAME, QString() );
        return image.save( fileName );
    }
    return false;
}

bool SavePlugin::executeFromGui()
{
    SaveDialog dialog;
    if ( dialog.exec() == QDialog::Accepted )
    {
        ParameterHash params;
        params.insert( IMAGE_NAME, dialog.image() );
        params.insert( FILENAME, dialog.filename() );
        return execute( params );
    }
    return false;
}

QVariant SavePlugin::data( int what ) const
{
    Q_UNUSED( what );
    return QVariant();
}

