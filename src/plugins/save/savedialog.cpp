/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "savedialog.h"
#include "ui_savedialog.h"

#include "imagecollector.h"
#include "imagefilterapplicationmixin.h"

SaveDialog::SaveDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SaveDialog)
{
    ui->setupUi(this);

    QStringList images = imageFilterApp->imageCollector()->images();
    foreach ( QString image, images )
    {
        ui->image->addItem( image );
    }
}

SaveDialog::~SaveDialog()
{
    delete ui;
}

QString SaveDialog::image() const
{
    return ui->image->currentText();
}

QString SaveDialog::filename() const
{
    return ui->filename->text();
}

void SaveDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
