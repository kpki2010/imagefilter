/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "resizeplugin.h"

#include "resizedialog.h"

#include "imagefilterapplicationmixin.h"
#include "imagecollector.h"

#include <QtPlugin>

Q_EXPORT_PLUGIN2( ResizePlugin, ResizePlugin )

const QString ResizePlugin::NAME = "resize";

const QString ResizePlugin::INPUT = "input";
const QString ResizePlugin::OUTPUT = "output";
const QString ResizePlugin::WIDTH = "width";
const QString ResizePlugin::HEIGHT = "height";
const QString ResizePlugin::MAX_WIDTH = "maxwidth";
const QString ResizePlugin::MAX_HEIGHT = "maxheight";
const QString ResizePlugin::MIN_WIDTH = "minwidth";
const QString ResizePlugin::MIN_HEIGHT = "minheight";
const QString ResizePlugin::SCALINGMODE = "scalingmode";
const QString ResizePlugin::ASPECTRATIOMODE = "aspectratiomode";

const QString ResizePlugin::SCALINGMODE_FAST = "fast";
const QString ResizePlugin::SCALINGMODE_SMOOTH = "smooth";

const QString ResizePlugin::ASPECTRATIOMODE_KEEP = "keep";
const QString ResizePlugin::ASPECTRATIOMODE_DISCARD = "discard";


const QString ResizePlugin::name() const
{
    return NAME;
}

void ResizePlugin::setCollection( FilterCollector *collection )
{
    m_filterCollector = collection;
}

const QStringList ResizePlugin::parameters() const
{
    QStringList result;
    result << INPUT
            << OUTPUT
            << WIDTH
            << HEIGHT
            << MAX_WIDTH
            << MAX_HEIGHT
            << MIN_WIDTH
            << MIN_HEIGHT
            << SCALINGMODE
            << ASPECTRATIOMODE;
    return result;
}

const QString ResizePlugin::parameterDescription( const QString &parameterName ) const
{
    if ( parameterName == INPUT )
    {
        return tr( "The input image to process." );
    } else if ( parameterName == OUTPUT )
    {
        return tr( "The name to save the output image as." );
    } else if ( parameterName == WIDTH )
    {
        return tr( "If set, the image will be scaled to that exact width." );
    } else if ( parameterName == HEIGHT )
    {
        return tr( "If set, the image will be scaled to that exact height." );
    } else if ( parameterName == MAX_WIDTH )
    {
        return tr( "Scale the image to this width, if its width is greater." );
    } else if ( parameterName == MAX_HEIGHT )
    {
        return tr( "Scale the image to this height, if its height is greater." );
    } else if ( parameterName == MIN_WIDTH )
    {
        return tr( "Scale the image to this width, if its width is less." );
    } else if ( parameterName == MIN_HEIGHT )
    {
        return tr( "Scale the image to this height, if its height is less." );
    } else if ( parameterName == SCALINGMODE )
    {
        return tr( "Determine, if the scaled image should be smoothed or not. Possible values: %1, %2" ).arg( SCALINGMODE_FAST ).arg( SCALINGMODE_SMOOTH );
    } else if ( parameterName == ASPECTRATIOMODE )
    {
        return tr( "Determine, if the aspect ratio of the input image should be kept. Possible values: " ).arg( ASPECTRATIOMODE_KEEP ).arg( ASPECTRATIOMODE_DISCARD );
    }
    return QString();
}

bool ResizePlugin::execute(const ParameterHash &params )
{
    QImage image = imageFilterApp->imageCollector()->image( params.value( INPUT ) );
    if ( !( image.isNull() ) )
    {
        Qt::TransformationMode scalingMode = Qt::FastTransformation;
        if ( params.value( SCALINGMODE ) == SCALINGMODE_SMOOTH )
        {
            scalingMode = Qt::SmoothTransformation;
        }
        Qt::AspectRatioMode arMode = Qt::IgnoreAspectRatio;
        if ( params.value( ASPECTRATIOMODE ) == ASPECTRATIOMODE_KEEP )
        {
            arMode = Qt::KeepAspectRatioByExpanding;
        }
        int          w    = params.value( WIDTH,      "0" ).toUInt(),
                     h    = params.value( HEIGHT,     "0" ).toUInt(),
                     maxW = params.value( MAX_WIDTH,  "0" ).toUInt(),
                     maxH = params.value( MAX_HEIGHT, "0" ).toUInt(),
                     minW = params.value( MIN_WIDTH,  "0" ).toUInt(),
                     minH = params.value( MIN_HEIGHT, "0" ).toUInt();
        if ( w > 0 && h > 0 )
        {
            image = image.scaled( w, h, arMode, scalingMode );
        } else if ( w > 0 )
        {
            image = image.scaledToWidth( w, scalingMode );
        } else if ( h > 0 )
        {
            image = image.scaledToHeight( h, scalingMode );
        } else
        {
            w = 0;
            h = 0;
            if ( maxW > 0 && image.width() > maxW )
            {
                w = maxW;
            }
            if ( maxH > 0 && image.height() > maxH )
            {
                h = maxH;
            }
            if ( minW > 0 && image.width() < minW )
            {
                w = minW;
            }
            if ( minH > 0 && image.height() < minH )
            {
                h = minH;
            }
            if ( w > 0  && h == 0 )
            {
                image = image.scaledToWidth( w, scalingMode );
            } else if ( w == 0 && h > 0 )
            {
                image = image.scaledToHeight( h, scalingMode );
            } else if ( w > 0 && h > 0 )
            {
                image = image.scaled( w, h, arMode, scalingMode );
            }
        }
        if ( params.contains( OUTPUT ) )
        {
            imageFilterApp->imageCollector()->setImage( params.value( OUTPUT ), image );
        }
    }
    return true;
}

bool ResizePlugin::executeFromGui()
{
    ResizeDialog dialog;
    if ( dialog.exec() == QDialog::Accepted )
    {
        ParameterHash params;
        params.insert( INPUT,           dialog.input() );
        params.insert( OUTPUT,          dialog.output() );
        params.insert( WIDTH,           dialog.imageWidth() );
        params.insert( HEIGHT,          dialog.imageHeight() );
        params.insert( MAX_WIDTH,       dialog.maxWidth() );
        params.insert( MAX_HEIGHT,      dialog.maxHeight() );
        params.insert( MIN_WIDTH,       dialog.minWidth() );
        params.insert( MIN_HEIGHT,      dialog.minHeight() );
        params.insert( SCALINGMODE,     dialog.scalingMode() );
        params.insert( ASPECTRATIOMODE, dialog.aspectRatioMode() );
        return execute( params );
    }
    return true;
}

QVariant ResizePlugin::data( int what ) const
{
    Q_UNUSED( what );
    return QVariant();
}

