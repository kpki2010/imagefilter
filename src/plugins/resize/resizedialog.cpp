/*
    PROJECTNAME - PROJECTDESCRIPTION
    Copyright (C) 2010  USER <EMAIL>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "resizedialog.h"
#include "ui_resizedialog.h"

#include "resizeplugin.h"

#include "imagecollector.h"
#include "imagefilterapplicationmixin.h"

#include <QCompleter>

ResizeDialog::ResizeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ResizeDialog)
{
    ui->setupUi(this);

    ui->input->addItems( imageFilterApp->imageCollector()->images() );
    ui->output->setCompleter( new QCompleter( imageFilterApp->imageCollector()->images(), this ) );
}

ResizeDialog::~ResizeDialog()
{
    delete ui;
}

const QString ResizeDialog::imageWidth() const
{
    return QString::number( ui->width->value() );
}

const QString ResizeDialog::imageHeight() const
{
    return QString::number( ui->height->value() );
}

const QString ResizeDialog::maxWidth() const
{
    return QString::number( ui->maxWidth->value() );
}

const QString ResizeDialog::maxHeight() const
{
    return QString::number( ui->maxHeight->value() );
}

const QString ResizeDialog::minWidth() const
{
    return QString::number( ui->minWidth->value() );
}

const QString ResizeDialog::minHeight() const
{
    return QString::number( ui->minHeight->value() );
}

const QString ResizeDialog::scalingMode() const
{
    if ( ui->fast->isChecked() )
    {
        return ResizePlugin::SCALINGMODE_FAST;
    } else
    {
        return ResizePlugin::SCALINGMODE_SMOOTH;
    }
}

const QString ResizeDialog::aspectRatioMode() const
{
    if ( ui->discard->isChecked() )
    {
        return ResizePlugin::ASPECTRATIOMODE_DISCARD;
    } else
    {
        return ResizePlugin::ASPECTRATIOMODE_KEEP;
    }
}

const QString ResizeDialog::input() const
{
    return ui->input->currentText();
}

const QString ResizeDialog::output() const
{
    return ui->output->text();
}

void ResizeDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
