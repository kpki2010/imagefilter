/*
    PROJECTNAME - PROJECTDESCRIPTION
    Copyright (C) 2010  USER <EMAIL>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RESIZEDIALOG_H
#define RESIZEDIALOG_H

#include <QDialog>

namespace Ui {
    class ResizeDialog;
}

class ResizeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ResizeDialog(QWidget *parent = 0);
    ~ResizeDialog();

    const QString imageWidth() const;
    const QString imageHeight() const;
    const QString maxWidth() const;
    const QString maxHeight() const;
    const QString minWidth() const;
    const QString minHeight() const;
    const QString scalingMode() const;
    const QString aspectRatioMode() const;
    const QString input() const;
    const QString output() const;

protected:
    void changeEvent(QEvent *e);

private:
    Ui::ResizeDialog *ui;
};

#endif // RESIZEDIALOG_H
