TEMPLATE = lib
CONFIG += plugin
CONFIG(debug, debug|release):DESTDIR = ../../../bin/Debug/plugins
else:DESTDIR = ../../../bin/Release/plugins
INCLUDEPATH += ../../interfaces
HEADERS += resizeplugin.h \
    resizedialog.h
SOURCES += resizeplugin.cpp \
    resizedialog.cpp
CONFIG(debug, debug|release):LIBS += -L../../../bin/Debug
else:LIBS += -L../../../bin/Release
LIBS += -limagefilterinterfaces

FORMS += \
    resizedialog.ui
