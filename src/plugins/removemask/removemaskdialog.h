#ifndef REMOVEMASKDIALOG_H
#define REMOVEMASKDIALOG_H

#include <QDialog>

namespace Ui {
    class RemoveMaskDialog;
}

class RemoveMaskDialog : public QDialog {
    Q_OBJECT
public:
    RemoveMaskDialog(QWidget *parent = 0);
    ~RemoveMaskDialog();

    const QString input() const;
    const QString output() const;
    const QString side() const;

protected:
    void changeEvent(QEvent *e);

private:
    Ui::RemoveMaskDialog *ui;
};

#endif // REMOVEMASKDIALOG_H
