/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "removemaskplugin.h"

#include "removemaskdialog.h"

#include "imagecollector.h"
#include "imagefilterapplicationmixin.h"

#include <QColor>
#include <QtPlugin>

Q_EXPORT_PLUGIN2( RemoveMaskPlugin, RemoveMaskPlugin )

const QString RemoveMaskPlugin::NAME = "removemask";

const QString RemoveMaskPlugin::INPUT       = "input";
const QString RemoveMaskPlugin::OUTPUT      = "output";
const QString RemoveMaskPlugin::SIDE        = "side";

const QString RemoveMaskPlugin::DIRECTION_TOP        = "top";
const QString RemoveMaskPlugin::DIRECTION_BOTTOM     = "bottom";
const QString RemoveMaskPlugin::DIRECTION_LEFT       = "left";
const QString RemoveMaskPlugin::DIRECTION_RIGHT      = "right";


const QString RemoveMaskPlugin::name() const
{
    return NAME;
}

void RemoveMaskPlugin::setCollection( FilterCollector *collection )
{
    m_filterCollector = collection;
}

const QStringList RemoveMaskPlugin::parameters() const
{
    QStringList result;
    result << INPUT
           << OUTPUT
           << SIDE;
    return result;
}

const QString RemoveMaskPlugin::parameterDescription( const QString &parameterName ) const
{
    if ( parameterName == INPUT )
    {
        return tr( "The input image to process." );
    } else if ( parameterName == OUTPUT )
    {
        return tr( "A name to save the resulting image as." );
    } else if ( parameterName == SIDE )
    {
        return tr( "The side of the image to start searching for a part of the mask to remove. Can be one of '%1', '%2', '%3', '%4'." )
                .arg( DIRECTION_TOP ).arg( DIRECTION_BOTTOM ).arg( DIRECTION_LEFT ).arg( DIRECTION_RIGHT );
    }
    return QString();
}

bool RemoveMaskPlugin::execute(const ParameterHash &params )
{
    bool rowwise;
    int current;
    int direction;
    QString side = params.value( SIDE, QString() );

    QImage result( imageFilterApp->imageCollector()->image( params.value( INPUT ) ) );
    if ( !( directionToParams( side, current, direction, rowwise, result.width(), result.height() ) ) )
    {
        return false;
    }

    while ( isCurrentOk( current, rowwise, result.width(), result.height() ) )
    {
        QPoint pixel( -1, -1 );
        if ( rowwise )
        {
            for ( int x = 0; x < result.width(); x++ )
            {
                if ( result.pixel( x, current ) == QColor( Qt::white ).rgb() )
                {
                    pixel = QPoint( x, current );
                    break;
                }
            }
        } else
        {
            for ( int y = 0; y < result.height(); y++ )
            {
                if ( result.pixel( current, y ) == QColor( Qt::white ).rgb() )
                {
                    pixel = QPoint( current, y );
                    break;
                }
            }
        }

        if ( ( pixel.x() >= 0 ) && ( pixel.y() >= 0 ) )
        {
            fill( &result, pixel );
            if ( params.contains( OUTPUT ) )
            {
                imageFilterApp->imageCollector()->setImage( params.value( OUTPUT ), result );
            }
            return true;
        }

        current = current + direction;
    }

    return true;
}

bool RemoveMaskPlugin::executeFromGui()
{
    RemoveMaskDialog dialog;
    if ( dialog.exec() == QDialog::Accepted )
    {
        ParameterHash params;
        params.insert( INPUT, dialog.input() );
        params.insert( OUTPUT, dialog.output() );
        params.insert( SIDE, dialog.side() );
        return execute( params );
    }
    return true;
}

QVariant RemoveMaskPlugin::data( int what ) const
{
    Q_UNUSED( what );
    return QVariant();
}

bool RemoveMaskPlugin::directionToParams( const QString &side, int &current, int &direction, bool &rowwise, int width, int height ) const
{
    if ( side == DIRECTION_TOP )
    {
        current = 0;
        direction = 1;
        rowwise = true;
    } else if ( side == DIRECTION_BOTTOM )
    {
        current = height - 1;
        direction = -1;
        rowwise = true;
    } else if ( side == DIRECTION_LEFT )
    {
        current = 0;
        direction = 1;
        rowwise = false;
    } else if ( side == DIRECTION_RIGHT )
    {
        current = width - 1;
        direction = -1;
        rowwise = false;
    } else
    {
        return false;
    }
    return true;
}

bool RemoveMaskPlugin::isCurrentOk( int current, bool rowwise, int width, int height ) const
{
    if ( rowwise )
    {
        return ( current >= 0 ) && ( current < height );
    } else
    {
        return ( current >= 0 ) && ( current < width );
    }
}

void RemoveMaskPlugin::fill( QImage *image, QPoint start ) const
{
    QList< QPoint > fillThese;
    fillThese.append( start );

    while ( !( fillThese.isEmpty() ) )
    {
        QPoint pixel = fillThese.first();
        fillThese.removeFirst();
        image->setPixel( pixel, QColor( Qt::black ).rgb() );

        QPoint neighbors[ 8 ] = { QPoint( -1, -1 ), QPoint( 0, -1 ), QPoint( 1, -1 ),
                                  QPoint( -1, 0 ),                   QPoint( 1, 0 ),
                                  QPoint( -1, 1 ),  QPoint( 0, 1 ),  QPoint( 1, 1 ) };
        for ( int i = 0; i < 8; i++ )
        {
            QPoint n = pixel + neighbors[ i ];
            if ( ( n.x() >= 0 ) && ( n.x() < image->width() ) && ( n.y() >= 0 ) && ( n.y() < image->height() ) && !( fillThese.contains( n ) ) )
            {
                if ( image->pixel( n ) == QColor( Qt::white ).rgb() )
                {
                    fillThese.append( n );
                }
            }
        }
    }
}
