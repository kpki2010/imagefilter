TEMPLATE = lib
CONFIG += plugin
CONFIG(debug, debug|release):DESTDIR = ../../../bin/Debug/plugins
else:DESTDIR = ../../../bin/Release/plugins
INCLUDEPATH += ../../interfaces
HEADERS += removemaskplugin.h \
    removemaskdialog.h
SOURCES += removemaskplugin.cpp \
    removemaskdialog.cpp
FORMS += removemaskdialog.ui
CONFIG(debug, debug|release):LIBS += -L../../../bin/Debug
else:LIBS += -L../../../bin/Release
LIBS += -limagefilterinterfaces
