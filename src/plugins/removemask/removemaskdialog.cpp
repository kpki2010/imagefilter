#include "removemaskdialog.h"
#include "ui_removemaskdialog.h"

#include "removemaskplugin.h"

#include "imagecollector.h"
#include "imagefilterapplicationmixin.h"

#include <QCompleter>

RemoveMaskDialog::RemoveMaskDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RemoveMaskDialog)
{
    ui->setupUi(this);

    ui->input->addItems( imageFilterApp->imageCollector()->images() );
    ui->output->setCompleter( new QCompleter( imageFilterApp->imageCollector()->images(), this ) );

    ui->side->addItem( RemoveMaskPlugin::DIRECTION_BOTTOM );
    ui->side->addItem( RemoveMaskPlugin::DIRECTION_LEFT );
    ui->side->addItem( RemoveMaskPlugin::DIRECTION_RIGHT );
    ui->side->addItem( RemoveMaskPlugin::DIRECTION_TOP );
}

RemoveMaskDialog::~RemoveMaskDialog()
{
    delete ui;
}

const QString RemoveMaskDialog::input() const
{
    return ui->input->currentText();
}

const QString RemoveMaskDialog::output() const
{
    return ui->output->text();
}

const QString RemoveMaskDialog::side() const
{
    return ui->side->currentText();
}

void RemoveMaskDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
