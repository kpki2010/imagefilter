/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REMOVEMASKPLUGIN_H
#define REMOVEMASKPLUGIN_H

#include "imagefilter.h"

#include <QObject>
#include <QStringList>

class QImage;

class RemoveMaskPlugin : public QObject, public ImageFilter
{

    Q_OBJECT
    Q_INTERFACES( ImageFilter )

public:

    static const QString NAME;

    static const QString INPUT;
    static const QString OUTPUT;
    static const QString SIDE;

    static const QString DIRECTION_TOP;
    static const QString DIRECTION_BOTTOM;
    static const QString DIRECTION_LEFT;
    static const QString DIRECTION_RIGHT;


    const QString name() const;
    void setCollection( FilterCollector *collection );
    const QStringList parameters() const;
    const QString parameterDescription( const QString &parameterName ) const;
    bool execute( const ParameterHash &params );
    bool executeFromGui();
    QVariant data( int what ) const;

private:

    FilterCollector *m_filterCollector;

    bool directionToParams( const QString &side, int &current, int &direction, bool &rowwise, int width, int height ) const;
    bool isCurrentOk( int current, bool rowwise, int width, int height ) const;
    void fill( QImage *image, QPoint start ) const;

};

#endif // REMOVEMASKPLUGIN_H
