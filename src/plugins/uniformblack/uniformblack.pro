TEMPLATE = lib
CONFIG += plugin
CONFIG(debug, debug|release):DESTDIR = ../../../bin/Debug/plugins
else:DESTDIR = ../../../bin/Release/plugins
INCLUDEPATH += ../../interfaces
HEADERS += uniformblackplugin.h \
    uniformblackdialog.h
SOURCES += uniformblackplugin.cpp \
    uniformblackdialog.cpp
FORMS += uniformblackdialog.ui
CONFIG(debug, debug|release):LIBS += -L../../../bin/Debug
else:LIBS += -L../../../bin/Release
LIBS += -limagefilterinterfaces
