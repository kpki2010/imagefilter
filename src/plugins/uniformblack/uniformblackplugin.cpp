/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "uniformblackplugin.h"

#include "uniformblackdialog.h"

#include "imagefilterapplicationmixin.h"
#include "imagecollector.h"

#include <QColor>
#include <qmath.h>
#include <QtPlugin>

#define sqr(a) (a*a)

Q_EXPORT_PLUGIN2( UniformBlackPlugin, UniformBlackPlugin )

const QString UniformBlackPlugin::NAME = "uniformblack";

const QString UniformBlackPlugin::INPUT_IMAGE = "in";
const QString UniformBlackPlugin::OUTPUT_IMAGE = "out";
const QString UniformBlackPlugin::THRESHOLD = "threshold";
const QString UniformBlackPlugin::MAXIMAL_DISTANCE = "maxdist";

const QString UniformBlackPlugin::name() const
{
    return NAME;
}

void UniformBlackPlugin::setCollection( FilterCollector *collection )
{
    m_filterCollector = collection;
}

const QStringList UniformBlackPlugin::parameters() const
{
    QStringList result;
    result << INPUT_IMAGE << OUTPUT_IMAGE << THRESHOLD << MAXIMAL_DISTANCE;
    result.sort();
    return result;
}

const QString UniformBlackPlugin::parameterDescription( const QString &parameterName ) const
{
    if ( parameterName == INPUT_IMAGE )
    {
        return tr( "Input image to process." );
    } else if ( parameterName == OUTPUT_IMAGE )
    {
        return tr( "Image to safe processed data to." );
    } else if ( parameterName == THRESHOLD )
    {
        return tr( "Color value (lightness) up to which color is assumed black." );
    } else if ( parameterName == MAXIMAL_DISTANCE )
    {
        return tr( "Maximal deviation until which color is assumed to be gray." );
    }
    return QString();
}

bool UniformBlackPlugin::execute(const ParameterHash &params )
{
    QImage image = imageFilterApp->imageCollector()->image( params.value( INPUT_IMAGE, QString() ) );
    QString output = params.value( OUTPUT_IMAGE, QString() );
    int threshold = params.value( THRESHOLD, QString( "128" ) ).toInt( 0, 0 );
    int maxDistance = params.value( MAXIMAL_DISTANCE, QString( "50" ) ).toInt( 0, 0 );

    if ( !( image.isNull() || output.isNull() ) )
    {
        for ( int x = 0; x < image.width(); x++ )
        {
            for ( int y = 0; y < image.height(); y++ )
            {
                QColor pixel = image.pixel( x, y );
                int dist = ( abs( pixel.red() - pixel.green() ) +
                             abs( pixel.green() - pixel.blue( ) ) ) / 2;
                int value = sqrt( sqr( pixel.red() ) + sqr( pixel.green() ) + sqr( pixel.blue() ) ) * 255 / sqrt(sqr(255)*3);
                if ( dist <=  maxDistance && value <= threshold )
                {
                    image.setPixel( x, y, QColor( Qt::black ).rgb() );
                } else
                {
                    image.setPixel( x, y, QColor( Qt::white ).rgb() );
                }
            }
        }
        imageFilterApp->imageCollector()->setImage( output, image );
        return true;
    }
    return false;
}

bool UniformBlackPlugin::executeFromGui()
{
    UniformBlackDialog dialog;
    if ( dialog.exec() == QDialog::Accepted )
    {
        ParameterHash params;
        params.insert( INPUT_IMAGE, dialog.input() );
        params.insert( OUTPUT_IMAGE, dialog.output() );
        params.insert( THRESHOLD, QString( "%1" ).arg( dialog.threshold() ) );
        params.insert( MAXIMAL_DISTANCE, QString( "%1" ).arg( dialog.maxDistance() ) );
        return execute( params );
    }
    return false;
}

QVariant UniformBlackPlugin::data( int what ) const
{
    Q_UNUSED( what );
    return QVariant();
}

