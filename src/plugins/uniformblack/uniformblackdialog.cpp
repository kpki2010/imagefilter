/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "uniformblackdialog.h"
#include "ui_uniformblackdialog.h"

#include "imagecollector.h"
#include "imagefilterapplicationmixin.h"

#include <QCompleter>

UniformBlackDialog::UniformBlackDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UniformBlackDialog)
{
    ui->setupUi(this);

    QStringList images = imageFilterApp->imageCollector()->images();
    foreach ( QString image, images )
    {
        ui->input->addItem( image );
    }
    ui->output->setCompleter( new QCompleter( images ) );
}

UniformBlackDialog::~UniformBlackDialog()
{
    delete ui;
}

QString UniformBlackDialog::input() const
{
    return ui->input->currentText();
}

QString UniformBlackDialog::output() const
{
    return ui->output->text();
}

int UniformBlackDialog::threshold() const
{
    return ui->threshold->value();
}

int UniformBlackDialog::maxDistance() const
{
    return ui->maxDistance->value();
}

void UniformBlackDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
