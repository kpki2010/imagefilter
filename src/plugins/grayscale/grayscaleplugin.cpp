/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "grayscaleplugin.h"

#include "grayscaledialog.h"

#include "imagefilterapplicationmixin.h"
#include "imagecollector.h"

#include <QColor>
#include <qmath.h>
#include <QtPlugin>

#define sqr(a) (a*a)
Q_EXPORT_PLUGIN2( GrayScalePlugin, GrayScalePlugin )

const QString GrayScalePlugin::NAME = "grayscale";

const QString GrayScalePlugin::INPUT_IMAGE = "in";
const QString GrayScalePlugin::OUTPUT_IMAGE = "out";

const QString GrayScalePlugin::name() const
{
    return NAME;
}

void GrayScalePlugin::setCollection( FilterCollector *collection )
{
    m_filterCollector = collection;
}

const QStringList GrayScalePlugin::parameters() const
{
    QStringList result;
    result << INPUT_IMAGE << OUTPUT_IMAGE;
    result.sort();
    return result;
}

const QString GrayScalePlugin::parameterDescription( const QString &parameterName ) const
{
    if ( parameterName == INPUT_IMAGE )
    {
        return tr( "Image to process" );
    } else if ( parameterName == OUTPUT_IMAGE )
    {
        return tr( "Image to save result to." );
    }
    return QString();
}

bool GrayScalePlugin::execute(const ParameterHash &params )
{
    QImage image = imageFilterApp->imageCollector()->image( params.value( INPUT_IMAGE, QString() ) );
    QString output = params.value( OUTPUT_IMAGE, QString() );
    if ( !( image.isNull() || output.isNull() ) )
    {
        for ( int x = 0; x < image.width(); x++ )
        {
            for ( int y = 0; y < image.height(); y++ )
            {
                QColor pixel = image.pixel( x, y );
                int value = sqrt( sqr( pixel.red() ) + sqr( pixel.green() ) + sqr( pixel.blue() ) ) * 255 / sqrt(sqr(255)*3);
                image.setPixel( x, y, QColor( value, value, value ).rgb() );
            }
        }
        imageFilterApp->imageCollector()->setImage( output, image );
        return true;
    }
    return false;
}

bool GrayScalePlugin::executeFromGui()
{
    GrayscaleDialog dialog;
    if ( dialog.exec() == QDialog::Accepted )
    {
        ParameterHash params;
        params.insert( INPUT_IMAGE, dialog.input() );
        params.insert( OUTPUT_IMAGE, dialog.output() );
        return execute( params );
    }
    return false;
}

QVariant GrayScalePlugin::data( int what ) const
{
    Q_UNUSED( what );
    return QVariant();
}

