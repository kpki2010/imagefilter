/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "crystalizeplugin.h"

#include "imagecollector.h"
#include "imagefilterapplicationmixin.h"

#include <QColor>
#include <qmath.h>
#include <QtPlugin>

#include <stdlib.h>

#define sqr(x) ((x)*(x))

Q_EXPORT_PLUGIN2( CrystalizePlugin, CrystalizePlugin )

const QString CrystalizePlugin::NAME = "crystalize";

const QString CrystalizePlugin::INPUT = "input";
const QString CrystalizePlugin::OUTPUT = "output";
const QString CrystalizePlugin::ITERATIONS_PER_PIXEL = "iterations";
const QString CrystalizePlugin::WRAP = "wrap";

const QString CrystalizePlugin::WRAPPING_TRUE = "true";
const QString CrystalizePlugin::WRAPPING_FALSE = "false";

const QString CrystalizePlugin::name() const
{
    return NAME;
}

void CrystalizePlugin::setCollection( FilterCollector *collection )
{
    m_filterCollector = collection;
}

const QStringList CrystalizePlugin::parameters() const
{
    QStringList result;
    result << INPUT << OUTPUT << ITERATIONS_PER_PIXEL << WRAP;
    return result;
}

const QString CrystalizePlugin::parameterDescription( const QString &parameterName ) const
{
    if ( parameterName == INPUT )
    {
        return tr( "The input image to process." );
    } else if ( parameterName == OUTPUT )
    {
        return tr( "The name to save the resulting image as." );
    } else if ( parameterName == ITERATIONS_PER_PIXEL )
    {
        return tr( "The number of iterations per pixel. This value heavily influences how the result looks like." );
    } else if ( parameterName == WRAP )
    {
        return tr( "Determines, whether the image shall be interpreted as tile or not. Can be %1 or %2" ).arg( WRAPPING_TRUE ).arg( WRAPPING_FALSE );
    }
    return QString();
}

bool CrystalizePlugin::execute(const ParameterHash &params )
{
    QImage image( imageFilterApp->imageCollector()->image( params.value( INPUT, QString() ) ) );
    if ( !( image.isNull() ) )
    {
        m_maxTime = params.value( ITERATIONS_PER_PIXEL, "1" ).toUInt();
        if ( m_maxTime < 1 )
        {
            m_maxTime = 1;
        }
        m_maxTime *= image.width() * image.height();
        m_currentTime = 1;
        unsigned int printInterval = qMax< unsigned int >( m_maxTime / 100, 1 ),
                     printCounter = 0,
                     progress = 0;

        m_wrap = params.value( WRAP, WRAPPING_FALSE ) == WRAPPING_TRUE;

        m_energy = energyOfImage( image );

        while ( m_currentTime <= m_maxTime )
        {
            if ( printCounter == 0 )
            {
                qDebug( "Progress: %d%%", progress );
                progress++;
                printCounter = printInterval;
            }
            printCounter--;
            // pick two pixels: One random and its (randomly selected) neighbor
            QPoint offsets[ 8 ] = { QPoint( -1, -1 ), QPoint( 0, -1 ), QPoint( 1, -1 ),
                                    QPoint( -1,  0 ),                  QPoint( 1,  0 ),
                                    QPoint( -1,  1 ), QPoint( 0,  1 ), QPoint( 1,  1 ) };
            QPoint p1( qrand() % image.width(), qrand() % image.height() );
            QPoint p2 = p1 + offsets[ qrand() % 8 ];
            CrystalizePlugin::wrapPoint( image, &p2 );

            // get energy of the pixels
            unsigned int ep1 = energyOfPixelAndNeighbors( image, p1 ),
                         ep2 = energyOfPixelAndNeighbors( image, p2 );

            // get colors
            QColor c1( image.pixel( p1 ) ),
                   c2( image.pixel( p2 ) );

            // swap pixels
            image.setPixel( p1, c2.rgba() );
            image.setPixel( p2, c1.rgba() );

            // get energy of pixels when swapped
            unsigned int sep1 = energyOfPixelAndNeighbors( image, p1 ),
                         sep2 = energyOfPixelAndNeighbors( image, p2 );

            // calculate new energy of image
            unsigned int energyNew = m_energy - ( ep1 + ep2 ) + ( sep1 + sep2 );

            // check whether to keep swap
            double decision = ( double ) qrand() / RAND_MAX;
            double probability = probabilityOfSwap( energyNew );
            //qDebug( "%d -> %d ? %f <= %f", m_energy, energyNew, decision, probability );
            if ( decision <= probability )
            {
                m_energy = m_energy - ( ep1 + ep2 ) + ( sep1 + sep2 );
            } else
            {
                image.setPixel( p1, c1.rgba() );
                image.setPixel( p2, c2.rgba() );
            }

            m_currentTime++;
        }
        qDebug( "Finished crystalization!" );

        imageFilterApp->imageCollector()->setImage( params.value( OUTPUT ), image );

    }
    return true;
}

bool CrystalizePlugin::executeFromGui()
{
    return execute( ParameterHash() );
}

QVariant CrystalizePlugin::data( int what ) const
{
    Q_UNUSED( what );
    return QVariant();
}

unsigned int CrystalizePlugin::energyOfPixel( const QImage &image, const QPoint &p ) const
{
    const QPoint overlay[ 8 ] = { QPoint( -1, -1 ), QPoint(  0, -1 ), QPoint( 1, -1 ),
                                  QPoint( -1,  0 ),                   QPoint( 1,  0 ),
                                  QPoint( -1,  1 ), QPoint(  0,  1 ), QPoint( 1,  1 ) };
    unsigned int energy = 0;
    QColor c( image.pixel( p ) );
    for ( int i = 0; i < 8; i++ )
    {
        QPoint n = p + overlay[ i ];
        wrapPoint( image, &n );
        QColor nc( image.pixel( n ) );
        energy += abs( c.red() - nc.red() ) + abs( c.green() - nc.green() ) +
                  abs( c.blue() - nc.blue() ) + abs( c.alpha() - nc.alpha() );
    }
    return energy;
}

unsigned int CrystalizePlugin::energyOfPixelAndNeighbors(const QImage &image, const QPoint &p) const
{
    const QPoint overlay[ 9 ] = { QPoint( -1, -1 ), QPoint(  0, -1 ), QPoint( 1, -1 ),
                                  QPoint( -1,  0 ), QPoint(  0,  0 ), QPoint( 1,  0 ),
                                  QPoint( -1,  1 ), QPoint(  0,  1 ), QPoint( 1,  1 ) };
    unsigned int result = 0;
    for ( int i = 0; i < 9; i++ )
    {
        QPoint n = p + overlay[ i ];
        wrapPoint( image, &n );
        result += energyOfPixel( image, n );
    }
    return result;
}



unsigned int CrystalizePlugin::energyOfImage( const QImage &image ) const
{
    unsigned int result = 0;
    if ( !( image.isNull() ) )
    {
        for ( int i = 0; i < image.width(); i++ )
        {
            for ( int j = 0; j < image.height(); j++ )
            {
                result += energyOfPixel( image, QPoint( i, j ) );
            }
        }
    }
    return result;
}

void CrystalizePlugin::wrapPoint( const QImage &image, QPoint *p ) const
{
    if ( !( image.isNull() ) )
    {
        if ( m_wrap )
        {
            while ( p->x() < 0 )
            {
                p->setX( p->x() + image.width() );
            }
            p->setX( p->x() % image.width() );
            while ( p->y() < 0 )
            {
                p->setY( p->y() + image.height() );
            }
            p->setY( p->y() % image.height() );
        } else
        {
            if ( p->x() < 0 )
            {
                p->setX( 0 );
            }
            if ( p->x() >= image.width() )
            {
                p->setX( image.width() - 1 );
            }
            if ( p->y() < 0 )
            {
                p->setY( 0 );
            }
            if ( p->y() >= image.height() )
            {
                p->setY( image.height() - 1 );
            }
        }
    }
}

double CrystalizePlugin::probabilityOfSwap( unsigned int newEnergy ) const
{
    if ( newEnergy < m_energy )
    {
        return 1.0;
    } else if ( m_currentTime == 0 || m_maxTime == 0 )
    {
        return 0.0;
    } else
    {
        double k = 0.00016339869; // = 1 / ( 255 * 3 * 8 )
        return 1.0 - exp( ( (double)m_energy - (double)newEnergy ) * k * ( 1 - sqr( (double ) m_currentTime / ( double )m_maxTime ) ) );
    }
}

