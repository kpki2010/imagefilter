/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CRYSTALIZEPLUGIN_H
#define CRYSTALIZEPLUGIN_H

#include "imagefilter.h"

#include <QObject>
#include <QStringList>

class CrystalizePlugin : public QObject, public ImageFilter
{

    Q_OBJECT
    Q_INTERFACES( ImageFilter )

public:

    static const QString NAME;

    static const QString INPUT;
    static const QString OUTPUT;
    static const QString ITERATIONS_PER_PIXEL;
    static const QString WRAP;

    static const QString WRAPPING_TRUE;
    static const QString WRAPPING_FALSE;

    const QString name() const;
    void setCollection( FilterCollector *collection );
    const QStringList parameters() const;
    const QString parameterDescription( const QString &parameterName ) const;
    bool execute( const ParameterHash &params );
    bool executeFromGui();
    QVariant data( int what ) const;

private:

    FilterCollector *m_filterCollector;

    unsigned int m_maxTime;
    unsigned int m_currentTime;
    unsigned int m_energy;
    bool         m_wrap;

    unsigned int energyOfPixel( const QImage &image, const QPoint &p ) const;
    unsigned int energyOfPixelAndNeighbors( const QImage &image, const QPoint &p ) const;
    unsigned int energyOfImage( const QImage &image ) const;
    void wrapPoint( const QImage &image, QPoint *p ) const;

    double probabilityOfSwap( unsigned int newEnergy ) const;



};

#endif // CRYSTALIZEPLUGIN_H
