/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "loadplugin.h"

#include "loaddialog.h"

#include "imagefilterapplicationmixin.h"
#include "imagecollector.h"

#include <QtPlugin>

Q_EXPORT_PLUGIN2( LoadPlugin, LoadPlugin )

const QString LoadPlugin::NAME = "load";

const QString LoadPlugin::FILENAME = "filename";
const QString LoadPlugin::IMAGE_NAME = "name";

const QString LoadPlugin::name() const
{
    return NAME;
}

void LoadPlugin::setCollection( FilterCollector *collection )
{
    m_filterCollector = collection;
}

const QStringList LoadPlugin::parameters() const
{
    QStringList result;
    result << FILENAME << IMAGE_NAME;
    result.sort();
    return result;
}

const QString LoadPlugin::parameterDescription( const QString &parameterName ) const
{
    if ( parameterName == FILENAME )
    {
        return tr( "A file name to read image from." );
    } else if ( parameterName == IMAGE_NAME )
    {
        return tr( "A name for accessing the loaded image." );
    }
    return QString();
}

bool LoadPlugin::execute(const ParameterHash &params )
{
    QImage image( params.value( FILENAME, QString() ) );
    if ( !( image.isNull() ) )
    {
        QString name = params.value( IMAGE_NAME, QString() );
        if ( !( name.isNull() ) )
        {
            imageFilterApp->imageCollector()->setImage( name, image );
            return true;
        }
    }
    return false;
}

bool LoadPlugin::executeFromGui()
{
    LoadDialog dialog;
    if ( dialog.exec() == QDialog::Accepted )
    {
        ParameterHash params;
        params.insert( FILENAME, dialog.filename() );
        params.insert( IMAGE_NAME, dialog.name() );
        return execute( params );
    }
    return false;
}

QVariant LoadPlugin::data( int what ) const
{
    Q_UNUSED( what );
    return QVariant();
}

