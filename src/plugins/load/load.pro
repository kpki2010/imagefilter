TEMPLATE = lib
CONFIG += plugin
CONFIG(debug, debug|release):DESTDIR = ../../../bin/Debug/plugins
else:DESTDIR = ../../../bin/Release/plugins
INCLUDEPATH += ../../interfaces
HEADERS += loadplugin.h \
    loaddialog.h
SOURCES += loadplugin.cpp \
    loaddialog.cpp
FORMS += loaddialog.ui
CONFIG(debug, debug|release):LIBS += -L../../../bin/Debug
else:LIBS += -L../../../bin/Release
LIBS += -limagefilterinterfaces
