/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "loaddialog.h"
#include "ui_loaddialog.h"

#include "imagecollector.h"
#include "imagefilterapplicationmixin.h"

#include <QCompleter>
#include <QFileDialog>

LoadDialog::LoadDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoadDialog)
{
    ui->setupUi(this);
    connect( ui->toolButton, SIGNAL(clicked()), this, SLOT(selectFile()) );

    ui->name->setCompleter( new QCompleter( imageFilterApp->imageCollector()->images() ) );
}

LoadDialog::~LoadDialog()
{
    delete ui;
}

QString LoadDialog::filename() const
{
    return ui->filename->text();
}

QString LoadDialog::name() const
{
    return ui->name->text();
}

void LoadDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void LoadDialog::selectFile()
{
    QString filename = QFileDialog::getOpenFileName( this, tr( "Load File" ) );
    if ( !( filename.isEmpty() ) )
    {
        ui->filename->setText( filename );
    }
}
