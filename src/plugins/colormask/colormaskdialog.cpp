#include "colormaskdialog.h"
#include "ui_colormaskdialog.h"

#include "imagecollector.h"
#include "imagefilterapplicationmixin.h"

#include <QColor>
#include <QCompleter>

ColorMaskDialog::ColorMaskDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ColorMaskDialog)
{
    ui->setupUi(this);

    QCompleter *completer = new QCompleter( imageFilterApp->imageCollector()->images(), this );
    ui->input->addItems( imageFilterApp->imageCollector()->images() );
    ui->output->setCompleter( completer );

    QRgb channels[ 6 ] = { 0xff0000, 0x00ff00, 0x0000ff, 0xffff00, 0xff00ff, 0x00ffff };
    for ( int i = 0; i < 6; i++ )
    {
        QColor color( channels[ i ] );
        ui->channel->addItem( QString( "0x%1" ).arg( channels[ i ], 6, 16, QLatin1Char( '0' ) ), color );
    }
}

ColorMaskDialog::~ColorMaskDialog()
{
    delete ui;
}

QColor ColorMaskDialog::channel() const
{
    return ui->channel->itemData( ui->channel->currentIndex() ).value< QColor >();
}

unsigned int ColorMaskDialog::treshold() const
{
    return static_cast< unsigned int >( ui->treshold->value() );
}

const QString ColorMaskDialog::input() const
{
    return ui->input->currentText();
}

const QString ColorMaskDialog::output() const
{
    return ui->output->text();
}

void ColorMaskDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
