#ifndef COLORMASKDIALOG_H
#define COLORMASKDIALOG_H

#include <QDialog>

namespace Ui {
    class ColorMaskDialog;
}

class ColorMaskDialog : public QDialog {
    Q_OBJECT
public:
    ColorMaskDialog(QWidget *parent = 0);
    ~ColorMaskDialog();

    QColor channel() const;
    unsigned int treshold() const;
    const QString input() const;
    const QString output() const;

protected:
    void changeEvent(QEvent *e);

private:
    Ui::ColorMaskDialog *ui;
};

#endif // COLORMASKDIALOG_H
