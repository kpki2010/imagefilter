/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "colormaskplugin.h"

#include "colormaskdialog.h"

#include "imagecollector.h"
#include "imagefilterapplicationmixin.h"

#include <QColor>
#include <QDebug>
#include <QRgb>
#include <QtPlugin>


Q_EXPORT_PLUGIN2( ColorMaskPlugin, ColorMaskPlugin )

const QString ColorMaskPlugin::NAME = "colormask";

const QString ColorMaskPlugin::CHANNEL      = "channel";
const QString ColorMaskPlugin::THRESHOLD    = "threshold";
const QString ColorMaskPlugin::INPUT        = "input";
const QString ColorMaskPlugin::OUTPUT       = "output";

const QString ColorMaskPlugin::name() const
{
    return NAME;
}

void ColorMaskPlugin::setCollection( FilterCollector *collection )
{
    m_filterCollector = collection;
}

const QStringList ColorMaskPlugin::parameters() const
{
    QStringList result;
    result << CHANNEL
           << THRESHOLD;
    return result;
}

const QString ColorMaskPlugin::parameterDescription( const QString &parameterName ) const
{
    if ( parameterName == CHANNEL )
    {
        return tr( "The color to extract. Should be a color value, e.g. 0xFF0000 for extracting red colors." );
    } else if ( parameterName == THRESHOLD )
    {
        return tr( "The minimal threshold a pixel must have of the given color. Should be a value out of 0 to 255." );
    } else if ( parameterName == INPUT )
    {
        return tr( "The input image to process." );
    } else if ( parameterName == OUTPUT )
    {
        return tr( "The image to save the output to." );
    }
    return QString();
}

bool ColorMaskPlugin::execute(const ParameterHash &params )
{
    unsigned int threshold = params.value( THRESHOLD, "75" ).toUInt();
    QColor color = QColor( QRgb( params.value( CHANNEL, "0x000000" ).toUInt( 0, 0 ) ) );

    if ( params.contains( INPUT ) )
    {
        QImage input = imageFilterApp->imageCollector()->image( params.value( INPUT ) );
        QImage result( input );
        for ( int x = 0; x < result.width(); x++ )
        {
            for ( int y = 0; y < result.height(); y++ )
            {
                QColor pixel = QColor( result.pixel( x, y ) );
                int selectedChannels        = 0,
                    otherChannels           = 0,
                    numSelectedChannels     = 0,
                    numOtherChannels        = 0;

                ( color.red()   == 0xff ? selectedChannels : otherChannels ) += pixel.red();
                ( color.green() == 0xff ? selectedChannels : otherChannels ) += pixel.green();
                ( color.blue()  == 0xff ? selectedChannels : otherChannels ) += pixel.blue();

                ( color.red()   == 0xff ? numSelectedChannels : numOtherChannels ) ++;
                ( color.green() == 0xff ? numSelectedChannels : numOtherChannels ) ++;
                ( color.blue()  == 0xff ? numSelectedChannels : numOtherChannels ) ++;

                bool isBackground;
                if ( numSelectedChannels == 0 )
                {
                    isBackground = true;
                } else if ( numOtherChannels == 0 )
                {
                    isBackground = false;
                } else
                {
                    isBackground = ( selectedChannels / numSelectedChannels - otherChannels / numOtherChannels ) <  static_cast< int >( threshold );
                }

                result.setPixel( x, y, QColor( isBackground ? Qt::black : Qt::white ).rgb() );
            }
        }
        if ( params.contains( OUTPUT ) )
        {
            imageFilterApp->imageCollector()->setImage( params.value( OUTPUT ), result );
        }
    }
    return true;
}

bool ColorMaskPlugin::executeFromGui()
{
    ColorMaskDialog dialog;
    if ( dialog.exec() == QDialog::Accepted )
    {
        ParameterHash params;
        params.insert( CHANNEL, QString( "0x%1" ).arg( dialog.channel().rgb(), 6, 16, QLatin1Char( '0' ) ) );
        params.insert( THRESHOLD, QString( "%1" ).arg( dialog.treshold() ) );
        params.insert( INPUT, dialog.input() );
        params.insert( OUTPUT, dialog.output() );
        return execute( params );
    }
    return true;
}

QVariant ColorMaskPlugin::data( int what ) const
{
    Q_UNUSED( what );
    return QVariant();
}

