#include "extractformdialog.h"
#include "ui_extractformdialog.h"

#include "imagecollector.h"
#include "imagefilterapplicationmixin.h"

#include <QCompleter>

ExtractFormDialog::ExtractFormDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ExtractFormDialog)
{
    ui->setupUi(this);

    ui->input->addItems( imageFilterApp->imageCollector()->images() );
    ui->mask->addItems( imageFilterApp->imageCollector()->images() );
    ui->output->setCompleter( new QCompleter( imageFilterApp->imageCollector()->images(), this ) );
}

ExtractFormDialog::~ExtractFormDialog()
{
    delete ui;
}

const  QString ExtractFormDialog::input() const
{
    return ui->input->currentText();
}

const QString ExtractFormDialog::mask() const
{
    return ui->mask->currentText();
}

const QString ExtractFormDialog::output()  const
{
    return ui->output->text();
}

void ExtractFormDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
