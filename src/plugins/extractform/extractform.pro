TEMPLATE = lib
CONFIG += plugin
CONFIG(debug, debug|release):DESTDIR = ../../../bin/Debug/plugins
else:DESTDIR = ../../../bin/Release/plugins
INCLUDEPATH += ../../interfaces
HEADERS += extractformplugin.h \
    extractformdialog.h
SOURCES += extractformplugin.cpp \
    extractformdialog.cpp
FORMS += extractformdialog.ui
CONFIG(debug, debug|release):LIBS += -L../../../bin/Debug
else:LIBS += -L../../../bin/Release
LIBS += -limagefilterinterfaces
