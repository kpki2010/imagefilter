/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "extractformplugin.h"

#include "extractformdialog.h"

#include "imagecollector.h"
#include "imagefilterapplicationmixin.h"

#include <QColor>
#include <QDebug>
#include <QImage>
#include <QPoint>
#include <qmath.h>
#include <QtPlugin>

//#define vecLength(v) sqrt( (double) ( v.x() * v.x() + v.y() * v.y() ) )
#define max( a, b ) ( a > b ? a : b )



double vecLength( const QPoint &p )
{
    return sqrt( ( double ) ( p.x() * p.x() + p.y() * p.y() ) );
}

Q_EXPORT_PLUGIN2( ExtractFormPlugin, ExtractFormPlugin )

const QString ExtractFormPlugin::NAME = "extractform";

const QString ExtractFormPlugin::INPUT      = "input";
const QString ExtractFormPlugin::MASK       = "mask";
const QString ExtractFormPlugin::OUTPUT     = "output";

const QString ExtractFormPlugin::name() const
{
    return NAME;
}

void ExtractFormPlugin::setCollection( FilterCollector *collection )
{
    m_filterCollector = collection;
}

const QStringList ExtractFormPlugin::parameters() const
{
    QStringList result;
    result << INPUT
           << MASK
           << OUTPUT;
    return result;
}

const QString ExtractFormPlugin::parameterDescription( const QString &parameterName ) const
{
    if ( parameterName == INPUT )
    {
        return tr( "The input image to process." );
    } else if ( parameterName == MASK )
    {
        return tr( "The input mask to use to get the border of the contained form." );
    } else if ( parameterName == OUTPUT )
    {
        return tr( "A name to save the resulting image as." );
    }
    return QString();
}

bool ExtractFormPlugin::execute(const ParameterHash &params )
{
    if ( params.contains( INPUT ) && params.contains( MASK ) )
    {
        QImage input = imageFilterApp->imageCollector()->image( params.value( INPUT ) );
        QImage mask = imageFilterApp->imageCollector()->image( params.value( MASK ) );
        QPoint topLeft( 0, 0 ),
               topRight( mask.width() - 1, 0 ),
               bottomLeft( 0, mask.height() - 1),
               bottomRight( mask.width() - 1, mask.height() - 1 );


        qDebug() << __FILE__ << __LINE__ << "Start finding corners.";
        findCorners( &mask, &topLeft, &topRight, &bottomLeft, &bottomRight );
        qDebug() << __FILE__ << __LINE__ << "Corners: " << topLeft << topRight << bottomLeft << bottomRight;

        qDebug() << __FILE__ << __LINE__ << "Finding center of form delimiters...";
        topLeft = centerOfRegion( &mask, topLeft );
        topRight = centerOfRegion( &mask, topRight );
        bottomLeft = centerOfRegion( &mask, bottomLeft );
        bottomRight = centerOfRegion( &mask, bottomRight );
        qDebug() << __FILE__ << __LINE__ << "Found centers:" << topLeft << topRight << bottomLeft << bottomRight;

        qDebug() << __FILE__ << __LINE__ << "Mapping to input coordinates...";
        mapToInput( &input, &mask, &topLeft, &topRight, &bottomLeft, &bottomRight );
        qDebug() << __FILE__ << __LINE__ << "Mapped coordinates: " << topLeft << topRight << bottomLeft << bottomRight;

        int width = ( int ) ceil( max( vecLength( topRight - topLeft ),
                                       vecLength( bottomRight - bottomLeft ) ) );
        int height = ( int ) ceil( max( vecLength( bottomLeft - topLeft ),
                                        vecLength( bottomRight - topRight ) ) );
        qDebug() << __FILE__ << __LINE__ << "Output image size: " << width << height;

        qDebug() << __FILE__ << __LINE__ << "Extracting form...";
        QImage result = QImage( QSize( width, height ), input.format() );
        extractForm( &input, &result, &topLeft, &topRight, &bottomLeft, &bottomRight );
        qDebug() << __FILE__ << __LINE__ << "Done!";

        if ( params.contains( OUTPUT ) )
        {
            imageFilterApp->imageCollector()->setImage( params.value( OUTPUT ), result );
        }

    }
    return true;
}

bool ExtractFormPlugin::executeFromGui()
{
    ExtractFormDialog dialog;
    if  ( dialog.exec() == QDialog::Accepted )
    {
        ParameterHash params;
        params.insert( INPUT, dialog.input() );
        params.insert( MASK, dialog.mask() );
        params.insert( OUTPUT, dialog.output() );
        return execute( params );
    }
    return true;
}

QVariant ExtractFormPlugin::data( int what ) const
{
    Q_UNUSED( what );
    return QVariant();
}

void ExtractFormPlugin::findCorners(QImage *image, QPoint *topLeft, QPoint *topRight,
                                    QPoint *bottomLeft, QPoint *bottomRight) const
{
    bool topLeftFound = false,
         topRightFound = false,
         bottomLeftFound = false,
         bottomRightFound = false;
    for ( int i = 0; i < vecLength( QPoint( image->width(), image->height() ) ); i++ )
    {
        int x = i,
            y = 0;
        while ( x >= 0 )
        {
            if ( !( topLeftFound ) )
            {
                if ( isMasked( image, x,  y ) )
                {
                    topLeft->setX( x );
                    topLeft->setY( y );
                    topLeftFound = true;
                }
            }

            if ( !( topRightFound ) )
            {
                if ( isMasked( image, image->width() - 1 - x, y ) )
                {
                    topRight->setX( image->width() - 1 - x );
                    topRight->setY( y );
                    topRightFound = true;
                }
            }

            if ( !( bottomLeftFound ) )
            {
                if ( isMasked( image, x, image->height() - 1 - y ) )
                {
                    bottomLeft->setX( x );
                    bottomLeft->setY( image->height() - 1 - y );
                    bottomLeftFound = true;
                }
            }

            if ( !( bottomRightFound ) )
            {
                if ( isMasked( image, image->width() - 1 - x, image->height() - 1 - y ) )
                {
                    bottomRight->setX( image->width() - 1 - x );
                    bottomRight->setY( image->height() - 1 - y );
                    bottomRightFound = true;
                }
            }

            if ( topLeftFound && topRightFound &&
                 bottomLeftFound && bottomRightFound )
            {
                return;
            }

            x--;
            y++;
        }
    }
}

void ExtractFormPlugin::mapToInput(QImage *input, QImage *mask,
                                   QPoint *topLeft, QPoint *topRight,
                                   QPoint *bottomLeft, QPoint *bottomRight) const
{
    if ( !( mask->isNull() ) )
    {
        topLeft->setX( topLeft->x() * input->width() / mask->width() );
        topLeft->setY( topLeft->y() * input->height() / mask->height() );

        topRight->setX( topRight->x() * input->width() / mask->width() );
        topRight->setY( topRight->y() * input->height() / mask->height() );

        bottomLeft->setX( bottomLeft->x() * input->width() / mask->width() );
        bottomLeft->setY( bottomLeft->y() * input->height() / mask->height() );

        bottomRight->setX( bottomRight->x() * input->width() / mask->width() );
        bottomRight->setY( bottomRight->y() * input->height() / mask->height() );
    }
}

void ExtractFormPlugin::extractForm(QImage *input, QImage *output,
                                    QPoint *topLeft, QPoint *topRight,
                                    QPoint *bottomLeft, QPoint *bottomRight) const
{
    for ( int x = 0; x < output->width(); x++ )
    {
        for ( int y = 0; y < output->height(); y++ )
        {
            QPoint p1 = *topLeft + ( *bottomLeft - *topLeft ) * y / output->height(),
                   p2 = *topRight + ( *bottomRight - *topRight ) *  y / output->height();
            QPoint p = p1 + ( p2 - p1 ) * x / output->width();
            output->setPixel( x, y, input->pixel( p ) );
        }
    }
}

bool ExtractFormPlugin::isMasked( QImage *image, int x, int y ) const
{
    if ( ( x >= 0 ) && ( x < image->width() ) &&
         ( y >= 0 ) && ( y < image->height() ) )
    {
        if ( image->pixel( x, y ) == QColor( Qt::white ).rgb() )
        {
            return true;
        }
    }
    return false;
}

QPoint ExtractFormPlugin::centerOfRegion( QImage *image, const QPoint &start ) const
{
    QList< QPoint > region, queue;
    QPoint neighbors[ 8 ] = { QPoint( -1, -1 ), QPoint( 0, -1 ), QPoint( 1, -1 ),
                              QPoint( -1, 0 ),                   QPoint( 1, 0 ),
                              QPoint( -1, 1 ),  QPoint( 0, 1 ),  QPoint( 1, 1 ) };
    queue.append( start );
    while ( !( queue.isEmpty() ) )
    {
        QPoint current = queue.first();
        queue.removeFirst();
        region.append( current );
        for ( int i = 0; i < 8; i++ )
        {
            QPoint n = current + neighbors[ i ];
            if ( isMasked( image, n.x(), n.y() ) )
            {
                if ( !( region.contains( n ) || queue.contains( n ) ) )
                {
                    queue.append( n );
                }
            }
        }
    }
    QPoint sum( 0, 0 );
    foreach ( QPoint p, region )
    {
        sum += p;
    }
    return sum / region.count();
}
