#ifndef EXTRACTFORMDIALOG_H
#define EXTRACTFORMDIALOG_H

#include <QDialog>

namespace Ui {
    class ExtractFormDialog;
}

class ExtractFormDialog : public QDialog {
    Q_OBJECT
public:
    ExtractFormDialog(QWidget *parent = 0);
    ~ExtractFormDialog();

    const QString input() const;
    const QString mask() const;
    const QString output() const;

protected:
    void changeEvent(QEvent *e);

private:
    Ui::ExtractFormDialog *ui;
};

#endif // EXTRACTFORMDIALOG_H
