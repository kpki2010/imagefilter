/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "meanplugin.h"

#include "meandialog.h"

#include "imagefilterapplicationmixin.h"
#include "imagecollector.h"

#include <QColor>
#include <QDebug>
#include <qmath.h>
#include <QPainter>
#include <QtPlugin>


Q_EXPORT_PLUGIN2( MeanPlugin, MeanPlugin )

const QString MeanPlugin::NAME = "mean";

const QString MeanPlugin::INPUT_IMAGE = "in";
const QString MeanPlugin::OUTPUT_IMAGE = "out";
const QString MeanPlugin::RADIUS = "radius";

const QString MeanPlugin::name() const
{
    return NAME;
}

void MeanPlugin::setCollection( FilterCollector *collection )
{
    m_filterCollector = collection;
}

const QStringList MeanPlugin::parameters() const
{
    QStringList result;
    result << INPUT_IMAGE << OUTPUT_IMAGE << RADIUS;
    result.sort();
    return result;
}

const QString MeanPlugin::parameterDescription( const QString &parameterName ) const
{
    if ( parameterName == INPUT_IMAGE )
    {
        return tr( "Source Image" );
    } else if ( parameterName == OUTPUT_IMAGE )
    {
        return tr( "Image to save result in." );
    } else if ( parameterName == RADIUS )
    {
        return tr( "Radius in pixel to use for blur." );
    }
    return QString();
}

bool MeanPlugin::execute(const ParameterHash &params )
{
    QImage image = imageFilterApp->imageCollector()->image( params.value( INPUT_IMAGE, QString() ) );
    if ( !( image.isNull() ) )
    {
        image = image.convertToFormat( QImage::Format_ARGB32_Premultiplied );
        QImage calculated( image );
        QString output = params.value( OUTPUT_IMAGE, QString() );
        int radius = params.value( RADIUS, QString( "1" ) ).toInt( 0, 0 );
        if ( !( output.isNull() ) )
        {
            for ( int x = 0; x < image.width(); x++ )
            {
                for ( int y = 0; y < image.height(); y++ )
                {

                    int sR = 0,
                        sG = 0,
                        sB = 0,
                        sA = 0,
                        sN = 0;
                    for ( int lx = qMax( 0, x - radius ); lx < qMin( x + radius, image.width() ); lx++ )
                    {
                        for ( int ly = qMax( 0, y - radius ); ly < qMin( y + radius, image.height() ); ly++ )
                        {
                            const uchar *pxl = image.scanLine( ly ) + lx * 4;
                            sR += pxl[ 0 ];
                            sG += pxl[ 1 ];
                            sB += pxl[ 2 ];
                            sA += pxl[ 3 ];
                            sN++;
                        }
                    }
                    uchar *pxl = calculated.scanLine( y ) + x * 4;
                    pxl[ 0 ] = sR / sN;
                    pxl[ 1 ] = sG / sN;
                    pxl[ 2 ] = sB / sN;
                    pxl[ 3 ] = sA / sN;
                }
            }

            imageFilterApp->imageCollector()->setImage( output, calculated );
            return true;
        }
    }
    return false;
}

bool MeanPlugin::executeFromGui()
{
    MeanDialog dialog;
    if ( dialog.exec() == QDialog::Accepted )
    {
        ParameterHash params;
        params.insert( INPUT_IMAGE, dialog.input() );
        params.insert( OUTPUT_IMAGE, dialog.output() );
        params.insert( RADIUS, QString( "%1" ).arg( dialog.radius() ) );
        return execute( params );
    }
    return false;
}

QVariant MeanPlugin::data( int what ) const
{
    Q_UNUSED( what );
    return QVariant();
}

