/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "imagefilterapplication.h"

#include "stdio.h"

/**
  * @brief Application entry point.
  *
  * Creates  a new console application class, which will parse the
  * arguments and calls the necessary filters as requested by the user.
  */
int main( int argc, char **argv )
{
    ImageFilterApplication *app = new ImageFilterApplication( argc, argv );
    app->initializeImageFilterApplication( app );
    int result = app->exec();
    delete app;
    return result;
}
