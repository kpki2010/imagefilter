/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IMAGEFILTERAPPLICATION_H
#define IMAGEFILTERAPPLICATION_H

#include "imagefilterapplicationmixin.h"

#include <QCoreApplication>

/**
  * @brief Application class for the command line interface.
  *
  * This is the application class that is started when the command line imagefilter
  * application is called. It includes the required ImageFilterApplicationMixin class
  * so that all filters will work.
  */
class ImageFilterApplication : public QCoreApplication, public ImageFilterApplicationMixin
{

    Q_OBJECT

public:

    ImageFilterApplication(int argc, char **argv );
    virtual ~ImageFilterApplication();

    int exec();

private:

    class ImageFilterApplicationPrivate;
    ImageFilterApplicationPrivate *d;

};

#endif // IMAGEFILTERAPPLICATION_H
