/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "filtercallarguments.h"

class FilterCallArguments::FilterCallArgumentsPrivate
{
public:

    FilterCallArgumentsPrivate( FilterCallArguments *args )
        : name( QString() ),
          parameters( ParameterHash() ),
          filterCallArguments( args )
    {
    }

    virtual ~FilterCallArgumentsPrivate()
    {
    }

    QString         name;
    ParameterHash   parameters;

private:

    FilterCallArguments *filterCallArguments;

};


FilterCallArguments::FilterCallArguments( QObject *parent )
    : QObject( parent ),
      d( new FilterCallArgumentsPrivate( this ) )
{
}

FilterCallArguments::~FilterCallArguments()
{
    delete d;
}

const QString &FilterCallArguments::name() const
{
    return d->name;
}

const ParameterHash &FilterCallArguments::parameters() const
{
    return d->parameters;
}

bool FilterCallArguments::isValid() const
{
    return !( d->name.isEmpty() );
}

void FilterCallArguments::setName( const QString &name )
{
    d->name = name;
}

void FilterCallArguments::setParameters( const ParameterHash &params )
{
    d->parameters = params;
}

void FilterCallArguments::parse( const QStringList &args )
{
    d->name = QString();
    d->parameters = ParameterHash();

    if ( args.length() > 0 && args.length() % 2 == 1)
    {
        d->name = args.at( 0 );
        for ( int i = 0; i < ( args.length() - 1 ) / 2; i++ )
        {
            QString pn = args.at( i * 2 + 1 );
            QString pv = args.at( i * 2 + 2 );
            if ( pn.startsWith( "--" ) )
            {
                pn = pn.remove( 0, 2 );
            }
            d->parameters.insert( pn, pv );
        }
    }
}

void FilterCallArguments::reset()
{
    d->name = QString();
    d->parameters = ParameterHash();
}
