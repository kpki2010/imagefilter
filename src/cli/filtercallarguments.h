/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FILTERCALLARGUMENTS_H
#define FILTERCALLARGUMENTS_H

#include "imagefilterdefinitions.h"

#include <QObject>
#include <QStringList>

/**
  * @brief A single call of a filter.
  *
  * This class encapsulates a single call of a filter, i.e.
  * the name of the filter to execute and the parameters to pass
  * to that filter.
  *
  * It provides methods for parsing command line parameters into
  * a single filter call.
  */
class FilterCallArguments : public QObject
{

    Q_OBJECT

public:

    FilterCallArguments( QObject *parent = 0 );
    virtual ~FilterCallArguments();

    const QString &name() const;
    const ParameterHash &parameters() const;
    bool isValid() const;

public slots:

    void setName( const QString &name );
    void setParameters( const ParameterHash &params );
    void parse( const QStringList &args );
    void reset();

private:

    class FilterCallArgumentsPrivate;
    FilterCallArgumentsPrivate *d;

};

#endif // FILTERCALLARGUMENTS_H
