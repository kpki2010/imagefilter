/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMMANDLINEPARSER_H
#define COMMANDLINEPARSER_H

#include <QList>
#include <QObject>
#include <QStringList>

class FilterCallArguments;

/**
  * @brief Command line parser.
  *
  * This class parses the command line and constructs a list of filter calls
  * from it.
  *
  * The assumed syntax for calling the program is:
  * programname {filtername { option value } }
  *
  * The parser will create a list which contains entries for each
  * filtername and the key-value list holding the options and their
  * values.
  */
class CommandLineParser : public QObject
{
public:
    CommandLineParser( QObject *parent = 0 );
    virtual ~CommandLineParser();

    /**
      * @brief The list of parsed filter calls.
      */
    const QList< FilterCallArguments * > &filterCalls() const;

    /**
      * @brief Parses command line arguments.
      *
      * Parses a list of command line arguments (as passed to the program at
      * startup) into a list of filter calls.
      *
      * The filters to be executed are available after this vall
      * via the filterCalls() method.
      *
      * @returns True on success, otherwise false.
      */
    bool parse( QStringList arguments );

private:

    class CommandLineParserPrivate;
    CommandLineParserPrivate *d;

};

#endif // COMMANDLINEPARSER_H
