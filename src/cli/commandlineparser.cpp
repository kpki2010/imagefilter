/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "commandlineparser.h"

#include "filtercallarguments.h"

#include <QDebug>

class CommandLineParser::CommandLineParserPrivate
{

public:

    CommandLineParserPrivate( CommandLineParser *parser )
        : commandLineParser( parser )
    {
    }

    virtual ~CommandLineParserPrivate()
    {
    }

    QList< FilterCallArguments * > filterCalls;

private:

    CommandLineParser *commandLineParser;

};

CommandLineParser::CommandLineParser( QObject *parent )
    : QObject( parent ),
      d( new CommandLineParserPrivate( this ) )
{
}

CommandLineParser::~CommandLineParser()
{
    delete d;
}

const QList< FilterCallArguments * > &CommandLineParser::filterCalls() const
{
    return d->filterCalls;
}

bool CommandLineParser::parse(QStringList arguments)
{
    while ( arguments.length() > 0 )
    {
        QStringList fargs;
        if ( arguments[ 0 ].indexOf( "-" ) == 0 )
        {
            qWarning() << "CommandLineParser: Expected filter name but got parameter name [" << arguments[ 0 ] << "]";
            return false;
        }
        fargs << arguments[ 0 ];
        arguments.removeFirst();
        while ( arguments.length() > 0 )
        {
            if ( arguments[ 0 ].indexOf( "--" ) == 0 )
            {
                QString paramName = arguments.first();
                arguments.removeFirst();
                if ( arguments.length() == 0 )
                {
                    qWarning() << "CommandLineParser: Expected parameter value but got end of line.";
                    return false;
                }
                QString paramValue = arguments.first();
                arguments.removeFirst();
                paramName = paramName.remove( 0, 2 );
                fargs << paramName << paramValue;
            } else
            {
                break;
            }
        }
        FilterCallArguments *fca = new FilterCallArguments( this );
        fca->parse( fargs );
        if ( fca->isValid() )
        {
            d->filterCalls.append( fca );
        } else
        {
            qWarning() << "CommandLineParser: Failed parsing filter call";
            return false;
        }
    }
    return true;
}
