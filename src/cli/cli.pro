TEMPLATE = app
TARGET = imagefilter
INCLUDEPATH += ../interfaces
CONFIG(debug, debug|release):LIBS += -L../../bin/Debug
else:LIBS += -L../../bin/Release
LIBS += -limagefilterinterfaces
CONFIG(debug, debug|release):DESTDIR = ../../bin/Debug
else:DESTDIR = ../../bin/Release
HEADERS += imagefilterapplication.h \
    filtercallarguments.h \
    commandlineparser.h
SOURCES += imagefilterapplication.cpp \
    main.cpp \
    filtercallarguments.cpp \
    commandlineparser.cpp
