/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "imagefilterapplication.h"

#include "commandlineparser.h"
#include "filtercallarguments.h"

#include "imagefilter.h"
#include "imagefilterdefinitions.h"
#include "filtercollector.h"

#include <QDebug>
#include <QString>
#include <QStringList>
#include <QTimer>

class ImageFilterApplication::ImageFilterApplicationPrivate
{

public:

    ImageFilterApplicationPrivate( ImageFilterApplication *application )
        : commandLineParser( new CommandLineParser( application ) ),
          imageFilterApplication( application )
    {
    }

    CommandLineParser *commandLineParser;
    bool               commandLineParserIsReady;

private:

    ImageFilterApplication *imageFilterApplication;

};

ImageFilterApplication::ImageFilterApplication( int argc, char**argv )
    : QCoreApplication( argc, argv ),
      ImageFilterApplicationMixin(),
      d( new ImageFilterApplicationPrivate( this ) )
{
    QStringList args = arguments();
    args.removeFirst();
    d->commandLineParserIsReady = d->commandLineParser->parse( args );
}

ImageFilterApplication::~ImageFilterApplication()
{
    delete d;
}

int ImageFilterApplication::exec()
{
    if ( !( d->commandLineParserIsReady ) )
    {
        return 1;
    }
    foreach ( FilterCallArguments *fca, d->commandLineParser->filterCalls() )
    {
        ImageFilter *f = filterCollector()->filter( fca->name() );
        if ( f )
        {
            if ( !( f->execute( fca->parameters() ) ) )
            {
                qWarning() << "ImageFilterApplication: Failed executing filter" << f->name();
                return 1;
            }
        } else
        {
            qWarning() << "ImageFilterApplication: No such filter:" << fca->name();
            return 1;
        }
    }
    return 0;
}
