/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "filtercollector.h"
#include "imagecollector.h"
#include "imagefilter.h"
#include "imagefilterapplicationmixin.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setupMenus();
    m_scene = new QGraphicsScene( this );
    ui->graphicsView->setScene( m_scene );

    connect( ui->listWidget, SIGNAL(currentRowChanged(int)), this, SLOT(showImage(int)) );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::setupMenus()
{
    QMenu *fileMenu = menuBar()->addMenu( tr( "File" ) );

    QAction *quitAction = fileMenu->addAction( tr( "Quit" ) );
    quitAction->setShortcut( QKeySequence::Quit );
    connect( quitAction, SIGNAL(triggered()), qApp, SLOT(quit()) );

    QMenu *filterMenu = menuBar()->addMenu( tr( "Filter" ) );
    foreach ( QString filterName, imageFilterApp->filterCollector()->filterNames() )
    {
        QAction *action = filterMenu->addAction( filterName );
        connect( action, SIGNAL(triggered()), this, SLOT(executeFilter()) );
    }

    QMenu *helpMenu = menuBar()->addMenu( tr( "Help" ) );

    QAction *aboutQtAction = helpMenu->addAction( tr( "About Qt" ) );
    connect( aboutQtAction, SIGNAL(triggered()), qApp, SLOT(aboutQt()) );
}

void MainWindow::updateImageList()
{
    ui->listWidget->clear();

    QStringList images = imageFilterApp->imageCollector()->images();
    foreach ( QString image, images )
    {
        ui->listWidget->addItem( image );
    }
}

void MainWindow::executeFilter()
{
    QAction *action = dynamic_cast< QAction * >( sender() );
    if ( action )
    {
        ImageFilter *filter = imageFilterApp->filterCollector()->filter( action->text() );
        if ( filter )
        {
            filter->executeFromGui();
            updateImageList();
        }
    }
}

void MainWindow::showImage( int index )
{
    m_scene->clear();
    if ( index >= 0 && index < ui->listWidget->count() )
    {
        QPixmap image = QPixmap::fromImage( imageFilterApp->imageCollector()->image( ui->listWidget->item( index )->text() ) );
        m_scene->setSceneRect( 0, 0, image.width(), image.height() );
        m_scene->addPixmap( image );
    }
}
