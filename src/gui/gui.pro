TEMPLATE = app
TARGET = imagefilter-gui
INCLUDEPATH += ../interfaces
QT += core gui
CONFIG(debug, debug|release):LIBS += -L../../bin/Debug
else:LIBS += -L../../bin/Release
LIBS += -limagefilterinterfaces
CONFIG(debug, debug|release):DESTDIR = ../../bin/Debug
else:DESTDIR = ../../bin/Release
HEADERS += imagefilterguiapplication.h \
    mainwindow.h
SOURCES += imagefilterguiapplication.cpp \
    main.cpp \
    mainwindow.cpp
FORMS += mainwindow.ui
