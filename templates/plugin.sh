#!/bin/bash
plugin=$(echo $1 | tr " " "_")
plugin_lower=$(echo $plugin | tr "[:upper:]" "[:lower:]")
plugin_upper=$(echo $plugin | tr "[:lower:]" "[:upper:]")

mkdir -p src/plugins/$plugin_lower

cat templates/plugin/plugin.pro | sed s/\\\$\\\$PLUGIN\\\$\\\$/$plugin/g | sed s/\\\$\\\$PLUGIN_LOWER\\\$\\\$/$plugin_lower/g | sed s/\\\$\\\$PLUGIN_UPPER\\\$\\\$/$plugin_upper/g > src/plugins/$plugin_lower/$plugin_lower.pro


cat templates/plugin/plugin.h | sed s/\\\$\\\$PLUGIN\\\$\\\$/$plugin/g | sed s/\\\$\\\$PLUGIN_LOWER\\\$\\\$/$plugin_lower/g | sed s/\\\$\\\$PLUGIN_UPPER\\\$\\\$/$plugin_upper/g > src/plugins/$plugin_lower/$(echo $plugin_lower)plugin.h

cat templates/plugin/plugin.cpp | sed s/\\\$\\\$PLUGIN\\\$\\\$/$plugin/g | sed s/\\\$\\\$PLUGIN_LOWER\\\$\\\$/$plugin_lower/g | sed s/\\\$\\\$PLUGIN_UPPER\\\$\\\$/$plugin_upper/g > src/plugins/$plugin_lower/$(echo $plugin_lower)plugin.cpp
