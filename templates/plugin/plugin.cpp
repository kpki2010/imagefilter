/*
    ImageFilter - Command Line Image filtering and processing
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "$$PLUGIN_LOWER$$plugin.h"

#include <QtPlugin>

Q_EXPORT_PLUGIN2( $$PLUGIN$$Plugin, $$PLUGIN$$Plugin )

const QString $$PLUGIN$$Plugin::NAME = "$$PLUGIN_LOWER$$";

const QString $$PLUGIN$$Plugin::name() const
{
    return NAME;
}

void $$PLUGIN$$Plugin::setCollection( FilterCollector *collection )
{
    m_filterCollector = collection;
}

const QStringList $$PLUGIN$$Plugin::parameters() const
{
    return QStringList();
}

const QString $$PLUGIN$$Plugin::parameterDescription( const QString &parameterName ) const
{
    return QString();
}

bool $$PLUGIN$$Plugin::execute(const ParameterHash &params )
{
    return true;
}

bool $$PLUGIN$$Plugin::executeFromGui()
{
    return execute( ParameterHash() );
}

QVariant $$PLUGIN$$Plugin::data( int what ) const
{
    return QVariant();
}

