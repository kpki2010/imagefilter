#!/bin/bash

PROFILE=Debug
if [ -n "$1" ]; then
  PROFILE=$1
fi

BIN_DIR=../../bin/$PROFILE
export PATH=$BIN_DIR:$PATH
export LD_LIBRARY_PATH=$BIN_DIR:$LD_LIBRARY_PATH

imagefilter load --filename ./scan.png --name input \
            colormask --channel 0xff0000 --threshold 75 --input input --output mask \
            mean --in mask --radius 2 --out mask \
            uniformblack --in mask --threshold 254 --out mask \
            removemask --input mask --side top --output mask \
            extractform --input input --mask mask --output output \
            save --name mask --filename ./mask.png \
            save --name output --filename ./output.png
            
            
